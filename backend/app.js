const express = require('express');  
const cors = require('cors')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')

const Task = require('./models/task')

const app = express()

const api_tasks_url = '/api/tasks'

mongoose.connect('mongodb://localhost:27017/todo_db', { useNewUrlParser: true })
  .then(() => {
    console.log(' +++ Connection established +++ ')
  })
  .catch(() => {
    console.log(' --- Some errors --- ')
  })

app.use(bodyParser.json())
app.use(cors({
    origin: true
}))


app.get(api_tasks_url, (req, res, next) => {

  Task.find().then((tasks => {
    console.log('got all tasks')
    res.status(200).json({tasks: tasks})
  }))

})

app.post(api_tasks_url, (req, res, next) => {

  const task = new Task({
    taskName: req.body.taskName,
    responsibleEmployee: req.body.responsibleEmployee,
    hourlyPlan: req.body.hourlyPlan
  })
  task.save().then(savedTask => {
    res.status(201).json({
      taskId: savedTask._id
    })
    console.log(savedTask._id +' is added')
  })

})

app.delete(api_tasks_url + '/:id', (req, res, next) => {
 
  Task.deleteOne({_id: req.params.id}).then(result => {
    res.status(200).send()
    console.log(req.params.id + ' is deleted')
  })
  
}) 


module.exports = app