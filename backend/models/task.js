const mongoose = require('mongoose')

const taskSchema = mongoose.Schema({
  taskName: {type: String, required: true},
  responsibleEmployee: {type: String, required: true},
  hourlyPlan: {type: String}
})


module.exports = mongoose.model('Task', taskSchema)