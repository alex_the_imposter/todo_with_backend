import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from "@angular/common/http";

import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { BtnDeleteComponent } from './todolist/todolist-item/btn-delete/btn-delete.component';
import { TodolistComponent } from './todolist/todolist.component';
import { TodolistItemComponent } from './todolist/todolist-item/todolist-item.component';
import { ReactiveFormsModule } from '@angular/forms';
import { TodolistFormComponent } from './todolist/todolist-form/todolist-form.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MaterialModule } from "./material.module";


@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    BtnDeleteComponent, 
    TodolistComponent,
    TodolistItemComponent,
    TodolistFormComponent,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
