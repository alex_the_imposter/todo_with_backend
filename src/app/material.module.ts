import { NgModule } from '@angular/core';

import {
    MatButtonModule,
    MatInputModule,
    MatGridListModule,
    MatDividerModule,
    MatCardModule,
    MatListModule,
    MatToolbarModule
} from '@angular/material';


@NgModule({
  imports: [],
  exports: [
    MatButtonModule,
    MatInputModule,
    MatGridListModule,
    MatDividerModule,
    MatCardModule,
    MatListModule,
    MatToolbarModule
  ],
})
export class MaterialModule { }