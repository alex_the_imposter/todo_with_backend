export class Task {
    constructor(
        public taskName: string, 
        public responsibleEmployee: string,
        public hourlyPlan?: string,
        public id?: string
    ) {}
}