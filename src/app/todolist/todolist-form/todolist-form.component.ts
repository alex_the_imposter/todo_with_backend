import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { TodolistService } from '../todolist.service';

@Component({
  selector: 'app-todolist-form',
  templateUrl: './todolist-form.component.html',
  styleUrls: ['./todolist-form.component.css']
})
export class TodolistFormComponent implements OnInit {

  private addTaskForm: FormGroup;
  @ViewChild('f') myNgForm;

  constructor(private todolistService: TodolistService, fb: FormBuilder) { 

    this.addTaskForm = fb.group({
      'taskName': ['', Validators.required],
      'employee': ['', Validators.required],
      'hourlyPlan': ['']
    })
  }

  ngOnInit() {
  }

  private addItem(){

    if (this.addTaskForm.valid) {
      const c = this.addTaskForm.controls;
      this.todolistService.addTask(
        c.taskName.value, 
        c.employee.value, 
        c.hourlyPlan.value
      );
      this.addTaskForm.reset();
      this.myNgForm.resetForm();
    } else {
      console.log('Форма не валидна')
    }
  }
}