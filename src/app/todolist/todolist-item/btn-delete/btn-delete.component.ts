import { Component, OnInit, HostListener, Input } from '@angular/core';
import { Task } from '../../task.model';
import { TodolistService } from '../../todolist.service';

@Component({
  selector: 'app-btn-delete',
  templateUrl: './btn-delete.component.html',
  styleUrls: ['./btn-delete.component.css'],
})
export class BtnDeleteComponent implements OnInit {

  @Input() taskToDelete: Task;

  constructor(private todolistService: TodolistService) { }

  ngOnInit() {
  }
  
  @HostListener('click') onDelete() {
    this.todolistService.deleteTask(this.taskToDelete)
  }
}
