import { Component, OnInit } from '@angular/core';

import { Task } from './task.model';
import { TodolistService } from './todolist.service';


@Component({
  selector: 'app-todolist',
  templateUrl: './todolist.component.html',
  styleUrls: ['./todolist.component.css'],
})
export class TodolistComponent implements OnInit {

  title: string = 'Todo list';
  tasks: Task[];

  constructor(private todolistService: TodolistService) { }
  
  ngOnInit() {
    this.todolistService.getTasks();
    this.todolistService.taskChanged.subscribe(
      (tasks: Task[]) => {

        this.tasks = tasks;
      }
    );
  }
}
