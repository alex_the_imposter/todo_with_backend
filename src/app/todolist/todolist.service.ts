import { Injectable } from '@angular/core';
import { Subject } from 'rxjs'
import { map } from "rxjs/operators";
import { HttpClient } from '@angular/common/http';

import { Task } from './task.model';

@Injectable({
  providedIn: 'root'
})
export class TodolistService {

  constructor(private http: HttpClient) { }

  private tasks: Task[] = [];
  
  testurl = 'http://localhost:3000/api/tasks'

  // для информирования о изменениях в this.tasks
  taskChanged = new Subject<Task[]>();

  getTasks() {

    this.http.get<{tasks: any}>(this.testurl)

      .pipe(map((data: any) => {
        return data.tasks.map(task => {
          // 
          return {
            id: task._id,
            taskName: task.taskName,
            responsibleEmployee: task.responsibleEmployee,
            hourlyPlan: task.hourlyPlan
          }
        })
      }))
      .subscribe(
        (data: any) => {
          this.tasks = data;
          this.taskChanged.next(this.tasks.slice());
        }
      );
  }

  addTask(taskName: string, whoIsResponsible: string, hourlyPlan?: string) {
    
    if (this.tasks.some((task) => { return task.taskName === taskName && 
                                           task.responsibleEmployee === whoIsResponsible})) {
                                            
      console.log('Такой элемент уже есть.')
    } else {

      const newTask = new Task(taskName, whoIsResponsible, hourlyPlan)
      this.http.post<{taskId: string}>(this.testurl, newTask).subscribe(
        (response => {
          newTask.id = response.taskId;
          this.tasks.push(newTask);
          // информируем о изменениях.
          this.taskChanged.next(this.tasks);
          console.log(response.taskId)
        })
      ) 
    }
  }

  deleteTask(task: Task) {

    this.http.delete(this.testurl + '/' + task.id).subscribe(
      () => {
        const index = this.tasks.indexOf(task);
        this.tasks.splice(index, 1);
        this.taskChanged.next(this.tasks);

        console.log('deleted')
      }
    )
  }
}